chrome.webNavigation.onDOMContentLoaded.addListener(function (details) {
    chrome.storage.local.get('usabiliticsApps', function(data) {
        if (data && data.usabiliticsApps) {
            DOMContentLoaded({
                details: details,
                apps: data.usabiliticsApps
            });
        }
    });
});

function DOMContentLoaded(data) {
    var url = data.details.url;
    var tabId = data.details.tabId;

    var parsedLocation = getLocation(url);
    var currentUrl = parsedLocation.protocol + '//' + parsedLocation.host;

    var app = data.apps.filter(function(app) {
        return app.url === currentUrl;
    })[0];

    if (app) {
         chrome.browserAction.setIcon({
             path: "u-letter-blue-record.png",
             tabId: tabId
         });

         chrome.tabs.executeScript(tabId, {file: "tracking-script.js"}, function() {
            if (chrome.runtime.lastError) {
                console.error(chrome.runtime.lastError.message);
            } else {
                chrome.tabs.sendMessage(tabId, {
                     appId: app.id,
                     scriptUrl: 'https://s3-us-west-1.amazonaws.com/test-bot-tracker/test-bot-tracker.local.js'
                 });
            }
        });
    }
}

function getLocation(path) {
    var l = document.createElement("a");
    l.href = path;
    return l;
}