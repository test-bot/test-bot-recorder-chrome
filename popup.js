
/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

$(function() {
    var appsView = $('#appsView');
    var loginView = $('#loginView');

    var logoutButton = $('#logoutButton');
    var linkedInLoginButton = $('#linkedInLogin');
    var loginMessage = $('#loginMessage');

    var serverUrl = $('#serverUrl').val();
    var authUrl = $('#authUrl').val();

    appsView.hide();
    loginView.show();

    var deferred = Q.defer();
    chrome.storage.local.get('accessToken', function (result) {
        deferred.resolve(result.accessToken);
    });

    deferred.promise.then(function (accessToken) {
        if (accessToken) {
            appsView.show();
            loginView.hide();
            loadApps(serverUrl);
        }
    });

    linkedInLoginButton.click(function () {
        var url = authUrl  + '/auth/app/linkedin?appId=' + chrome.runtime.id;
        chrome.identity.launchWebAuthFlow({url: url, interactive: true}, function (callbackUrl) {
            var accessToken = getParameterByName('accessToken', callbackUrl);
            if (accessToken) {
                chrome.storage.local.set({accessToken, accessToken});
                appsView.show();
                loginView.hide();
                loadApps(serverUrl);
            } else {
                loginMessage.text('Invalid login.');
            }
        });
    });

    logoutButton.click(function () {
        chrome.storage.local.set({accessToken: null});
        appsView.hide();
        loginView.show();
    });
});

function loadApps (serverUrl) {
    if (serverUrl.charAt(serverUrl.length - 1) !== '/') {
        serverUrl += '/';
    }

    var appsEndpoint = serverUrl + 'v1/apps';

    var deferred = Q.defer();
    chrome.storage.local.get('accessToken', function (token) {
        deferred.resolve(token.accessToken);
    });

    deferred.promise
    .then(function (token) {
        return $.get({
            url:appsEndpoint,
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", 'Bearer ' + token);
            }
        })
    })
    .then(function (data) {
        $('#appList').empty();

        for (var i = 0; i < data.items.length; i++) {
            var app = data.items[i];
            var elem = $('<li class="list-group-item"/>').text(app.name);
            elem.append('<br/>').append($('<span/>').text(app.url));
            $('#appsList').append(elem);
        }

        return data.items;
    })
    .then(function(apps) {
        chrome.storage.local.set({usabiliticsApps: apps});
    })
    .catch(function (error) {
        throw error;
    });
}

function getParameterByName(name, url) {
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getLocation(path) {
    var l = document.createElement("a");
    l.href = path;
    return l;
}