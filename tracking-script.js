chrome.runtime.onMessage.addListener(function(message) {
    var usabiliticsSettings = {
        appId: message.appId,
        visitedTimestamp: Date.now()
    };

    var dataElem = document.createElement('div');
    dataElem.style.display='none';
    dataElem.id = 'usabiliticsSettingsNode';
    dataElem.textContent = JSON.stringify(usabiliticsSettings);

    document.getElementsByTagName('body')[0].appendChild(dataElem);

    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = message.scriptUrl;
    document.getElementsByTagName('head')[0].appendChild(s);
});